#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
Interface for the Bluff App Helper on UNIX platforms.
"""
from Tkinter import Tk, Listbox, BOTH, W, N, E, S, END
from ttk import Frame, Button, Label
from main import *
import tkSimpleDialog, tkMessageBox, pickle, subprocess

"""
appData is the data structure that manages the list of entries.
It also facilitates the saving and loading of data from the disk.
"""
class appData:
    
    """
    Constructor. Loads data from disk.
    """
    def __init__(self):
        try:
            self.names = []
            f = open("save.pkl", "rb")
            self.names = pickle.load(f)
        
        except IOError as err:
            print "No savefile could be found."

    """
    Adds an entry to the list by name.
    """
    def addName(self, name):
        self.names.append(name)
    
    """
    Removes an entry from the list by name.
    """
    def removeName(self, name):
        self.names.remove(name)
    
    """
    Accessor function that returns the list of entries.
    """
    def getNames(self):
        return self.names
        
    """
    Accessor function that returns the number of names.
    """
    def getCount(self):
        return len(self.names)
    
    """
    Saves the current list to disk. Directly called in the case that the "Quit"
    button is pressed.
    """
    def saveNames(self):
        pickle.dump(self.names, open("save.pkl", "wb"))
    
    """
    Accessor function that returns a boolean value as to whether or not name is 
    within the list of entries.
    """
    def find(self, name):
        return name in self.names
    
    """
    Accessor function that returns the entry at position n.
    """
    
    def getNameAtPosition(self, n):
        return self.names[n]
    
    """
    Deconstructor. Uses the saveNames function to save the list of entries to
    disk.
    """
    def __del__(self):
        self.saveNames()
    
"""
generateUI creates the UI for adding and removing entries from what is to be 
synchronized with the Pebble smartwatch.
"""
class generateUI(Frame):
  
    """
    Creates the storage and initializes the UI through the initUI.
    """
    def __init__(self, data, manager, parent):
        Frame.__init__(self, parent)   
        self.storage = data
        self.manager = manager
        self.parent = parent
        self.initUI()
    
    """
    initUI creates the gridded interface of the app.
    """
    def initUI(self):
        self.parent.title("Bluff Helper App")
        self.pack(fill=BOTH, expand=1)
        
        lbl = Label(self, text="Bluff Helper App") #NYI
        lbl.grid(sticky=W, pady=5, padx=5)
        
        self.area = Listbox(self)
        self.area.grid(row=1, column=0, columnspan=2, rowspan=4, 
            padx=5, pady=5, sticky=E+W+S+N)
        
        previousStorage = self.storage.getNames()
        
        for k in previousStorage:
            self.area.insert(END,k)
        
        sbtn = Button(self, text="Sync",
            command=self.manager.sync)
        sbtn.grid(row=1, column=3)
        
        abtn = Button(self, text="Add",
            command=self.addName)
        abtn.grid(row=2, column=3)
        
        abtn = Button(self, text="Remove",
            command=self.removeName)
        abtn.grid(row=3, column=3)

        qbtn = Button(self, text="Quit",
            command=self.quit)
        qbtn.grid(row=4, column=3)
        
    """
    The default quit functionality is overrided to force the helper app to save
    before quitting. The superclass' quit function is called.
    """
    def quit(self):
        self.storage.saveNames()
        Frame.quit(self)
    
    """
    addName doesn't take an argument: it prompts a dialog box to take the
    argument for the entry's name.
    """
    def addName(self):
        if self.storage.getCount() == 16:
            tkMessageBox.showwarning("Error!", 
                "You've reached the maximum number of entries!")
            return
        
    #The following segment verifies whether the command is valid.
        name = tkSimpleDialog.askstring("Add new item", "Name")
        p1 = subprocess.Popen(["ls", "/usr/bin/"], 
            stdout=subprocess.PIPE)
        p2 = subprocess.Popen(["grep", "-wo", name], stdin=p1.stdout, 
            stdout=subprocess.PIPE)
        p1.stdout.close()
        output = p2.communicate()[0]
        
        if output != "":
            if self.storage.find(name):
                tkMessageBox.showwarning("Error!", 
                    "This entry is already in your list!")
            else:
                self.storage.addName(name)
                self.area.insert(END,name)                
        else:
            tkMessageBox.showwarning("Error!", 
                "The binary you have entered cannot be found..")
    
    """
    Facilitates the removal of the entry from both the Listbox and appData.
    """
    def removeName(self):
        currentSelection = self.area.curselection()
        for i in currentSelection:
            name = self.area.get(i)
            self.area.delete(i,i)
            self.storage.removeName(name)
            
class IOManager:
    def __init__(self, data, parent):
        self.data = data
        self.parent = parent
        self.pebble = Pebble(peb_id)
        self.pull()

    def sync(self):
        tempList = self.data.getNames()
        newList = [item + '\0' for item in tempList]
        print newList
        app_message_tuples_of_tuples(self.pebble, uuid, 'CSTRING',
                 newList)
    
    def pull(self):
        endpoint, resp = self.pebble._recv_message()
        if endpoint != 48 or resp == None:
            print "endpoint != 48 or resp == None."
        else:
            key, val = decode_int(resp)
            if key == 1:
                self.sync()
            elif key == 2:
                print('Launching application with value {0}'.format(val))
                subprocess.call([self.data.getNameAtPosition(val)])
            
        self.parent.after(0, self.pull)

    def __del__(self):
        self.pebble.disconnect()

def main():
    root = Tk()
    root.resizable(width=False, height=False)

    storage = appData()
    manager = IOManager(storage, root)
    app = generateUI(storage, manager, root)
    
    root.after(0, manager.pull)
    root.mainloop()

if __name__ == '__main__':
    main()  
