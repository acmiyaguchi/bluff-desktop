from libpebble.pebble.pebble import Pebble, AppMessage
from struct import unpack
import time
import subprocess

peb_id='00:17:E9:6E:C8:54'
uuid = '1f8c6a91eda940d9ab1c9e47a749a4e9'
MAX_ATTEMPTS = 5
applications = ['chromium', 'movie', 'nemo']


def app_message_tuples_of_tuples(pebble, app_uuid, tuple_datatype, tuple_data):
    for value in tuple_data:
        pebble.app_message_send_tuple(uuid, 64, tuple_datatype, value)
        print('Sent {0}'.format(value))
        time.sleep(1)

def decode_int(resp):
    '''Decode the response given by a app message enpoint
    The 19:23 position represents the key, and the 27:30 position
    represents the give value of a integer tuple'''
    try:
        key = unpack('i', resp[19:23])[0]
        val = unpack('i', resp[-4:])[0]
        return key, val
    except Exception as e:
        return None, None

def main():
    attempts = 0
    while True:
        if attempts > MAX_ATTEMPTS:
            raise 'Could not connect to Pebble'
        try:
            pebble = Pebble(peb_id)
            break
        except:
            time.sleep(5)
            attempts += 1

    try:
        print("Connect it ")
        while True:
            endpoint, resp = pebble._recv_message()
            if endpoint != 48 or resp == None:
            	#time.sleep(.5)
                continue
            key, val = decode_int(resp)
            print('{0}:{1}'.format(key, val))
            # REQUEST_APPS
            if key == 1: 
                print('Sending dictionary to python')
                send_app = [item+'\0' for item in applications]
                app_message_tuples_of_tuples(pebble, uuid, 'CSTRING', send_app)
            # SEND_ID
            elif key == 2:
                print('Launching application with value {0}'.format(val))
                # Lets force the screen back on if we have to
                subprocess.call(['xset', 'dpms', 'force', 'on'])
                subprocess.call([applications[val]])

            else:
                 continue

    except Exception as e:
        pebble.disconnect()
        raise e
        return

if __name__ == '__main__':
    main()
